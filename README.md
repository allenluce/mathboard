# The Math Board

This is for giving classroom mathematics lectures. It supports
multiple panels operating at once.

## Features so far

- Draws with a single touch (only)
- Draws with stylus
- Erases with finger
- Scroll vertically (only) with two (or more) fingers
- Page up/down or go to the top/bottom with side buttons
- Run on multiple panels at once
- Long-press green to number panels
- Swipe on green to send current view to next panel(s)
  - I.e. if you're on panel 1, swipe to send to panel 2.
    - Panel 2 will send to panel 3, etc.

## Planned features

- Long press gives you a box to:
  - Clear screen
  - Change pen color
  - Pull assets out of
- Make eraser visible
- Maybe make page boundaries visible?
- Lasso select of items, move as you see fit.

Eventually make the thing savable.

Also:

 - Have "build," "instruct," and "student" modes.
 - Based on layers with some properties:
   - Locked (i.e. everything in this layer is locked to this layer)

If you are in BUILD mode you can designate which layer you are in
Instruct mode just means to lock certain layers. You decide which layer you are actually in?
Student mode means to lock certain other layers.

Each mode has a:
 - Default target layer
 - Set of layers that are locked for this mode
 - Set of layers that contain cloneable objects for this mode
 - Certain layers visible/invisible?
 - Access to certain assets in the popbox
