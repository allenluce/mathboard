package com.prometheanworld.mathboard

import android.graphics.Paint
import android.graphics.Path
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import org.locationtech.jts.geom.*
import org.locationtech.jts.operation.distance3d.PlanarPolygon3D

// A set of coordinates with some utility functions, like preparing
class Stroke(@Transient var paint: Paint): ArrayList<Float>(), java.io.Serializable {
  @Transient var path: Path? = null
    get() {
      if (field == null) { // Generate path
        field = Path()
        if (size >= 4) {
          field!!.moveTo(this[0], this[1])
          for(i in 2 until size - 1 step 2) {
            field!!.lineTo(this[i], this[i+1])
          }
        }
      }
      return field
    }

  // Invalidate the path when things are added.
  override fun addAll(elements: Collection<Float>): Boolean {
    path = null
    return super.addAll(elements)
  }

  private fun writeObject(outStream: ObjectOutputStream) {
    outStream.defaultWriteObject()
    outStream.writeInt(paint.color)
    outStream.writeFloat(paint.strokeWidth)
  }

  private fun readObject(inStream: ObjectInputStream) {
    inStream.defaultReadObject()
    paint = newPaint(inStream.readInt(), inStream.readFloat())
  }

  // Move this stroke by the given offset. Return largest Y seen.
  fun offset(x: Float, y: Float): Int {
    var maxY = -Int.MAX_VALUE
    for (i in 0 until size-1 step 2) {
      this[i] = this[i] + x
      this[i+1] = this[i+1] + y
      if(this[i+1] > maxY) {
        maxY = this[i+1].toInt()
      }
    }
    this.path = null
    return maxY
  }

  // Create and return a copy
  fun copy(): Stroke {
    var newList = Stroke(this.paint)
    newList.addAll(this.filterNotNull())
    return newList
  }

  // Is the x,y point contained within the polygon traced by us?
  fun contains(x: Float, y: Float): Boolean {
    var oddNodes = false
    var j = size - 2
    for (i in 0 until size-1 step 2) {
      if ((this[i+1] < y && this[j+1] >= y || this[j+1] < y && this[i+1] >= y)
           && (this[i] <= x || this[j] <= x)) {
        if (this[i]+(y-this[i+1])/(this[j+1]-this[i+1])*(this[j]-this[i]) < x) {
          oddNodes = !oddNodes
        }
      }
      j = i
    }
    return oddNodes
  }

	private fun segmentPoint(p0: Coordinate , p1: Coordinate , d0: Double, d1: Double): Coordinate {
		if (d0 <= 0) {
      return Coordinate(p0)
    }
		if (d1 <= 0) {
      return Coordinate(p1)
    }

		val f = Math.abs(d0) / (Math.abs(d0) + Math.abs(d1))
		val intx = p0.x + f * (p1.x - p0.x)
		val inty = p0.y + f * (p1.y - p0.y)
		val intz = p0.getZ() + f * (p1.getZ() - p0.getZ())
		return Coordinate(intx, inty, intz)
	}

  fun contains(inLine: Stroke): Boolean {
    if (size < 8) {
      return false
    }
    val gf = GeometryFactory()
    val points = arrayOfNulls<Coordinate>(size/2)
    // set points
    var n = 0
    for (i in 0 until size step 2) {
      points[n++] = Coordinate(this[i].toDouble(), this[i+1].toDouble(), 0.0)
    }

    val linePoints = arrayOfNulls<Coordinate>(inLine.size/2)
    n = 0
    for (i in 0 until inLine.size step 2) {
      linePoints[n++] = Coordinate(inLine[i].toDouble(), inLine[i+1].toDouble(), 0.0)
    }

    val csf = gf.getCoordinateSequenceFactory()
    val cs = csf.create(linePoints)

    val line = LineString(cs, gf)

    val jtsRing = gf.createLinearRing(points)
    val polyS = gf.createPolygon(jtsRing, null)
    val poly = PlanarPolygon3D(polyS)
		val seq = line.getCoordinateSequence()

		// start point of line
		val p0 = Coordinate()
		seq.getCoordinate(0, p0);
		var d0 = poly.getPlane().orientedDistance(p0)

		// for each segment in the line
		val p1 = Coordinate()

		for (i in 0 until seq.size() - 1) {
			seq.getCoordinate(i, p0)
			seq.getCoordinate(i + 1, p1)
			val d1 = poly.getPlane().orientedDistance(p1);

			/**
			 * If the oriented distances of the segment endpoints have the same sign,
			 * the segment does not cross the plane, and is skipped.
			 */
			if (d0 * d1 > 0) {
				continue
      }

			/**
			 * Compute segment-plane intersection point
			 * which is then used for a point-in-polygon test.
			 * The endpoint distances to the plane d0 and d1
			 * give the proportional distance of the intersection point
			 * along the segment.
			 */
			val intPt = segmentPoint(p0, p1, d0, d1)
			// Coordinate intPt = polyPlane.intersection(p0, p1, s0, s1);
			if (poly.intersects(intPt)) {
				return true // intPt
			}

			// shift to next segment
			d0 = d1
		}
		return false
	}
}

// A collection of strokes
class StrokeList(): MutableMap<String, Stroke> by linkedMapOf<String, Stroke>() {
  // Add offset to all strokes. Return largest Y seen.
  fun offset(x: Float, y: Float): Int {
    var maxY = -Int.MAX_VALUE
    for ((_, stroke) in this) {
      val mY = stroke.offset(x, y)
      if (mY > maxY) {
        maxY = mY
      }
    }
    return maxY
  }
}
