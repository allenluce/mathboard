package com.prometheanworld.mathboard

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.GestureDetector
import android.view.GestureDetector.OnGestureListener
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.PopupMenu

class SideButtons(context: Context): OnGestureListener, View.OnTouchListener {
  private var gestureDetector: GestureDetector
  private var board: BoardView? = null
  lateinit private var view: View
  
  init {
    gestureDetector = GestureDetector(context, this)
  }

  fun setBoard(boardView: BoardView?) {
    board = boardView
  }

  // Required to fulfill interface
  override fun onShowPress(e: MotionEvent) = Unit
  override fun onDown(e: MotionEvent) = true
  override fun onScroll(e1: MotionEvent, e2: MotionEvent, distanceX: Float, distanceY: Float) = false

  // Send to next screen
  override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
    if (view.id == R.id.button_network) {
      board!!.sendPosition()
    }
    return true
  }

  override fun onSingleTapUp(e: MotionEvent): Boolean {
    when (view.id) {
      R.id.button_top -> board!!.scrollTo(0,0)
      R.id.button_end -> board!!.toEnd()
      R.id.button_up -> board!!.pageUp()
      R.id.button_down -> board!!.pageDown()
      R.id.button_undo -> board!!.undo()
      R.id.button_redo -> board!!.redo()
      R.id.button_network -> board!!.sendPosition()
      R.id.button_lasso -> setLassoMode()
    }
    return true
  }

  fun setLassoMode() {
    val btn = (view.context as Activity).findViewById<Button>(R.id.button_lasso)
    board!!.lassoMode = !board!!.lassoMode
    btn.alpha = if (board!!.lassoMode) {
      .5f
    } else {
      1f
    }
  }

  
  override fun onLongPress(e: MotionEvent) {
    if (view.id == R.id.button_network) {
      val popup = PopupMenu(view.context, view)
      with (popup.menu) {
        for (i in 1..board!!.numBoards) {
          add("${i}")
        }
        add("reconnect")
      }
      popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item: MenuItem? ->
        if (item.toString() == "reconnect") {
          Log.w(TAG, "RECONNECTING")
          board!!.reconnect()
        } else {
          board!!.bid = "${item}".toInt()
          (view as Button).setText("${item}")
        }
        true
      })
      popup.show()
    }
  }
  
  override fun onTouch(v: View, event: MotionEvent): Boolean {
    view = v
    return gestureDetector.onTouchEvent(event)
  }
}
