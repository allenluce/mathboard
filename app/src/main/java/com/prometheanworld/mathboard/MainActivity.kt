package com.prometheanworld.mathboard

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.util.AttributeSet

val TAG = "YUP"

class MainActivity : Activity() {
  public var boardView: BoardView? = null
  
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.main)
    boardView = findViewById(R.id.view_whiteboard) as BoardView?

    val sideButtons = SideButtons(applicationContext)
    sideButtons.setBoard(boardView)

    for (button in arrayOf(R.id.button_network, R.id.button_up,
                           R.id.button_down, R.id.button_top,
                           R.id.button_end, R.id.button_undo,
                           R.id.button_redo, R.id.button_lasso)) {
      findViewById<Button>(button)!!.setOnTouchListener(sideButtons)
    }
  }

  override fun onDestroy () {
    boardView!!.onDestroy()
    super.onDestroy()
  }
}
