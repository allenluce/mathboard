package com.prometheanworld.mathboard

import kotlinx.coroutines.*
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Room
import androidx.room.Insert
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import androidx.room.Database
import androidx.room.RoomDatabase

import com.google.gson.reflect.TypeToken
import com.google.gson.Gson

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.DashPathEffect
import android.graphics.Paint
import android.graphics.Paint.Style
import android.graphics.PointF
import android.os.StrictMode
import android.util.AttributeSet
import android.util.Log
import android.view.GestureDetector
import android.view.GestureDetector.OnGestureListener
import android.view.MenuItem
import android.view.MotionEvent
import android.view.SurfaceView
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import org.jgroups.JChannel
import org.jgroups.Message
import org.jgroups.Receiver
import org.jgroups.Address

import java.io.ByteArrayOutputStream
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.io.OutputStream

// Paint types
val PEN = 0
val LASSO = 1
val LASSOFILL = 2

// States -- what we're doing between down and up.
val DRAWING = 0
val ERASING = 1
val MOVING = 2
val SCROLLING = 3

// Message types
val STROKE = 0
val CLEAR = 1
val SCROLL = 2
val REMOVE = 3

fun ByteArray.toHexString() = asUByteArray().joinToString("") { it.toString(16).padStart(2, '0') }
fun String.hexToByteArray() = ByteArray(this.length / 2) { this.substring(it * 2, it * 2 + 2).toInt(16).toByte() }

fun newPaint(color: Int, strokeWidth: Float): Paint {
  val newPaint = Paint()
  newPaint.color = color
  newPaint.strokeWidth = strokeWidth
  newPaint.isAntiAlias = true
  newPaint.style = Paint.Style.STROKE
  newPaint.strokeJoin = Paint.Join.ROUND
  newPaint.strokeCap = Paint.Cap.ROUND
  return newPaint
}

class BoardView(context: Context, attributeSet: AttributeSet): SurfaceView(context, attributeSet), OnGestureListener, GestureDetector.OnDoubleTapListener, Receiver {
  val eraserWidth = 100f
  private var paints = arrayOf(newPaint(Color.BLACK, 3f), // PEN
                               newPaint(Color.BLUE, 3f), // LASSO
                               newPaint(Color.BLUE, 0f)) // LASSOFILL

  private var fingerMode = ERASING
  var lassoMode = false
    set(value) {
      if (field != value) {
        if (field) { // Turning off lasso.
          clearLasso()
          setLayerType(View.LAYER_TYPE_HARDWARE, null)
        }
        field = value
        if (field) { // Turning on lasso.
          currentStroke = Stroke(paints[LASSO])
          setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        }
      }
    }

  private var strokes = StrokeList()
  private var currentStroke = Stroke(paints[PEN])
  private var lassoed = StrokeList()
  private var undoStack = Undo()

  private var gestureDetector: GestureDetector
  private var channel: JChannel
  private var itsMe = true // Ignore my own broadcasts
  var maxY = 0 // Largest Y we've drawn
  var down = true // We're between a DOWN event and UP event
  var tag = "" // This board's unique part of the network stroke name
  var bid: Int = 0 // This board's ID

  var numBoards: Int = 0
    get() {
      return channel.view.size()
    }
  
  init {
    gestureDetector = GestureDetector(context, this)
    val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
    StrictMode.setThreadPolicy(policy)

    // Connect to other boards
    channel = JChannel(context.getAssets().open("jgroups.xml"))
    channel.setReceiver(this)
    channel.connect("MathBoard", null, 6000000)
    tag = channel.address.toString()
    // Set lasso-related paints
    val dashPath = DashPathEffect(floatArrayOf(5f, 5f), 1f)
    paints[LASSO].pathEffect = dashPath
    paints[LASSOFILL].alpha = 50
    paints[LASSOFILL].style = Paint.Style.FILL
  }

  fun reconnect() {
    channel.disconnect()
    channel = JChannel(context.getAssets().open("jgroups.xml"))
    channel.setReceiver(this)
    channel.connect("MathBoard", null, 6000000)
    tag = channel.address.toString()
  }

  // Send to all connected boards
  fun broadcast(vararg objects: Any) {
    val rawData = ByteArrayOutputStream()
    val s = ObjectOutputStream(rawData)
    for (obj in objects) {
      s.writeObject(obj)
    }
    s.close()
    channel.send(Message(null, rawData.toByteArray().toHexString()))
  }

  // Deal with another board's broadcast
  override fun receive(msg:Message) {
    if (msg.src == channel.address) { // Ignore myself
      return                                                
    }
    try {
      itsMe = false // Don't rebroadcast actions
      val rawData = (msg.getObject() as String).hexToByteArray()
      var s = ObjectInputStream(ByteArrayInputStream(rawData))
      val msgType = s.readObject() as Int
      when (msgType) {
        STROKE -> addStroke(s)
        CLEAR -> clear()
        SCROLL -> scroll(s.readObject() as Int, s.readObject() as Int)
        REMOVE -> removeStroke(s)
      }
      s.close()
    } finally {
      itsMe = true
    }
  }

  // Send our DB state to a new member
  override fun getState(output: OutputStream) {
    val s = ObjectOutputStream(output)
    synchronized (strokes) {
      for ((key, stroke) in strokes) {
        s.writeObject(key)
        s.writeObject(stroke)
      }
      s.writeObject("COMPLETED")
    }
    s.close()
  }

  // Receive DB state updates from an existing member
  override fun setState(input: InputStream) {
    val myPrefix = "${channel.address}-"
    val s = ObjectInputStream(input)
    synchronized (strokes) {
      while (true) {
        val key = s.readObject() as String
        if (key.startsWith(myPrefix)) { // Is this from me?
          val seq = key.removePrefix(myPrefix).toInt()
          if (seq > n) { // Make sure I'm counting above it.
            n = seq
          }
        }
        if (key == "COMPLETED") {
          break
        }
        val stroke = s.readObject() as Stroke
        strokes.put(key, stroke)
      }
    }
    s.close()
    n++ // Start with a higher stroke number than I've seen
  }

  private fun addStroke(s: ObjectInputStream) {
    val key = s.readObject() as String
    val stroke = s.readObject() as Stroke
    synchronized (strokes) {
      strokes.put(key, stroke)
    }
    invalidate()
  }

  private fun removeStroke(s: ObjectInputStream) {
    val strokeId = s.readObject() as String
    synchronized (strokes) {
      strokes.remove(strokeId)
    }
    invalidate()
  }

  public fun onDestroy() {
    channel.close()
  }

  // Clear the whole board
  fun clear() {
    synchronized (strokes) {
      strokes.clear()
    }
    invalidate()
    if (itsMe) {
      broadcast(CLEAR)
    }
  }

  override fun onDraw(cvs: Canvas) {
    parent.requestDisallowInterceptTouchEvent(true) // Keep continuing touches from aborting the draw
    synchronized (strokes) {
      for ((_, stroke) in strokes) {
        cvs.drawPath(stroke.path!!, stroke.paint)
      }
    }

    if (currentStroke.size >= 4) {
      cvs.drawPath(currentStroke.path!!, currentStroke.paint)
    }

    if (lassoMode && !down) { // Fill completed lasso
      cvs.drawPath(currentStroke.path!!, paints[LASSOFILL])
    }

    if (state == ERASING) { // Draw erase cursor
      val p = newPaint(Color.RED, 1f)
      val dashPath = DashPathEffect(floatArrayOf(5f, 5f), 1f)
      p.setPathEffect(dashPath)
      cvs.drawCircle(prevPoint.x, prevPoint.y, eraserWidth/2, p)
    }
  }

  override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float) = false
  override fun onShowPress(e: MotionEvent) = Unit
  override fun onLongPress(e: MotionEvent) = Unit
  override fun onSingleTapUp(e: MotionEvent) = false
  override fun onDoubleTapEvent(e: MotionEvent) = false
  override fun onSingleTapConfirmed(e: MotionEvent) = false
  // Pop up menu of thingies.
  override fun onDoubleTap(e: MotionEvent): Boolean {
    val root = getParent().getParent() as ViewGroup
    val v = View(context)
    v.setLayoutParams(ViewGroup.LayoutParams(1, 1))
    v.setBackgroundColor(Color.TRANSPARENT)
    root.addView(v)
    val popup = PopupMenu(v.context, v)
    with (popup.menu) {
      add("Clear")
      add(if (fingerMode == PEN) {
            "Switch finger to eraser"
          } else {
            "Switch finger to pen"
      })
      add("Save")
      add("Load")
    }
    v.setX(e.x)
    v.setY(e.y)
    popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item: MenuItem? ->
      when(item.toString()) {
        "Clear" -> clear()
        "Switch finger to eraser" -> fingerMode = ERASING
        "Switch finger to pen" -> fingerMode = PEN
        "Save" -> {
          GlobalScope.launch {
            context.deleteDatabase("strokebase")
            var db = Room.databaseBuilder(context, StrokeDatabase::class.java, "strokebase").build()
            for ((key, stroke) in strokes) {
              db.strokeDao().insertAll(StrokeData(key, stroke))
            }
            db.close()
          }
        }
        "Load" -> {
          GlobalScope.launch {
            var db = Room.databaseBuilder(context, StrokeDatabase::class.java, "strokebase").build()
            strokes.clear()
            for (strokeData in db.strokeDao().getAll()) {
              strokes.put(strokeData.id, strokeData.stroke)
            }
            db.close()
          }
        }
      }
      true
    })
    popup.show()
    return true
  }
  
  override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float) = false

  fun undo() {
    undoStack.undo()
    invalidate()
  }

  fun redo() {
    undoStack.redo()
    invalidate()
  }

  fun toEnd() {
    if (maxY-height > 0) {
      scrollTo(0, maxY-height+20)
      invalidate()
    }
  }
  
  fun pageUp() {
    scrollBy(0, -height)
    if (scrollY < 0) {
      scrollTo(0, 0)
    }
    invalidate()
  }
  
  fun pageDown() {
    if (maxY > scrollY) {
      scrollBy(0, height)
      invalidate()
    }
  }
  
  fun scroll(boardId: Int, y: Int) {
    if (boardId+1 == bid) { // It's for me!
      broadcast(SCROLL, bid, scrollY) // Send my position to the next board
      scrollTo(0, y)
    }
  }

  fun sendPosition() {
    if (scrollY >= 0) {
      broadcast(SCROLL, bid, scrollY) // Send position to the next board
    }
  }

  override fun onDown(event: MotionEvent) = processEvent(event)
  override fun onTouchEvent(event: MotionEvent): Boolean {
    if (gestureDetector.onTouchEvent(event)) {
      return true
    }
    return processEvent(event)
  }

  private var _sy: Int = 0
  private var prevPoint = PointF(0f, 0f)
  private var downPoint = PointF(0f, 0f)
  private var state = DRAWING

  private fun setYMax(y: Int) {
    if (y > maxY) {
      maxY = y
    }
  }

  private fun setYMax(start: PointF, end: PointF) {
    maxY = when {
      start.y.toInt() > maxY -> start.y.toInt()
      end.y.toInt() > maxY -> end.y.toInt()
      else -> maxY
    }
  }

  private var n = 0 // Stroke counter.
  private fun commitDraw(stroke: Stroke) {
    // Commit a new path (add it to the undo stack)
    val key = "${tag}-${n++}"
    val doit = fun() {
      synchronized (strokes) {
        strokes.put(key, stroke)
      }
      broadcast(STROKE, key, stroke)
    }
    val undo = fun() {
      synchronized (strokes) {
        strokes.remove(key)
      }
      broadcast(REMOVE, key)
    }
    undoStack.doit(doit, undo)
  }

  private fun commitErase(key: String) {
    val stroke = strokes[key]
    val doit = fun() {
      synchronized (strokes) {
        strokes.remove(key)
      }
      broadcast(REMOVE, key)
    }
    val undo = fun() {
      synchronized (strokes) {
        strokes.put(key, stroke!!)
      }
      broadcast(STROKE, key, stroke!!)
    }
    undoStack.doit(doit, undo)
  }

  fun markLassoed() {
    for ((key, stroke) in strokes) {
      if (currentStroke.contains(stroke)) {
        stroke.paint = paints[LASSO]
        lassoed.put(key, stroke)
      }
    }
  }


  val toDel = linkedMapOf<String, Stroke>()
  val toAdd = linkedMapOf<String, Stroke>()

  fun eraseMove(p: PointF) {
    var removeEm = StrokeList() // To avoid concurrency issues
    var addEm = StrokeList()
    // Are we colliding with a line?
    val radSquared = (eraserWidth/2) * (eraserWidth/2)
    for ((key, stroke) in strokes) {
      val origStroke = stroke.copy()
      var inv = false // Invalidate stroke?
      for (i in 0 until stroke.size-1 step 2) {
        val x = stroke[i]
        val y = stroke[i+1]
        if ((p.x - x)*(p.x - x) + (p.y - y)*(p.y - y) < radSquared) {
          // Yes! Mark the segment deleted.
          stroke[i] = -1f
          stroke[i+1] = -1f
          inv = true
        }
      }
      if (inv) {
        removeEm.put(key, origStroke)// And remove from local DB
        // Create new strokes
        var newStroke = Stroke(paints[PEN])
        for (i in 0 until stroke.size-1 step 2) {
          if (stroke[i] != -1f) {
            newStroke.addAll(arrayOf(stroke[i], stroke[i+1]))
          } else {
            if (newStroke.size >= 2) { // Drop empties or single points
              addEm.put("${tag}-${n++}", newStroke) // and add to local DB
              newStroke = Stroke(paints[PEN])
            }
          }
        }
        if (newStroke.size >= 2) {
          addEm.put("${tag}-${n++}", newStroke) // and add to local DB
        }
      }
    }
    for ((key, stroke) in removeEm) {
      strokes.remove(key) // Remove from local db
      toDel.put(key, stroke) // add to master del list
    }
    for ((key, stroke) in addEm) {
      strokes.put(key, stroke) // Add to local db
      toAdd.put(key, stroke) // add to master add list
    }
    invalidate()
  }

  private fun eraseUp() {
    val prunedAdds = StrokeList()
    val prunedDels = StrokeList()

    // Go through toAdd, skipping any strokes also in toDel
    for ((key, stroke) in toAdd) {
      if (!toDel.contains(key)) {
        prunedAdds.put(key, stroke)
        broadcast(STROKE, key, stroke)
      }
    }

    // Go through toDel, skipping any strokes also in toAdd
    for ((key, stroke) in toDel) {
      if (!toAdd.contains(key)) {
        prunedDels.put(key, stroke)
        broadcast(REMOVE, key)
      }
    }
    
    val doit = fun() {
      // First, add.
      for ((key, stroke) in prunedAdds) {
        strokes.put(key, stroke)
        broadcast(STROKE, key, stroke)
      }
      // Then remove.
      for ((key, _) in prunedDels) {
        strokes.remove(key)
        broadcast(REMOVE, key)
      }
    }
    val undo = fun() {
      // First, add.
      for ((key, stroke) in prunedDels) {
        strokes.put(key, stroke)
        broadcast(STROKE, key, stroke)
      }
      // Then remove.
      for ((key, _) in prunedAdds) {
        strokes.remove(key)
        broadcast(REMOVE, key)
      }
    }
    undoStack.silentDoit(doit, undo)
    state = DRAWING
  }

  fun processEvent(event: MotionEvent): Boolean {
    val touchPoint = PointF(event.x, event.y+scrollY)
    if (event.pointerCount == 1) { // Single finger/stylus
      when (event.actionMasked) {
        MotionEvent.ACTION_DOWN -> run {
          down = true
          downPoint = touchPoint
          if (lassoMode) {
            // Did we click in the lasso area?
            if (currentStroke.contains(touchPoint.x, touchPoint.y)) {
              // Start a lasso move
              state = MOVING
              return@run // Break
            }
          } else {
            when (event.getToolType(0)) {
              MotionEvent.TOOL_TYPE_STYLUS -> state = DRAWING
              MotionEvent.TOOL_TYPE_FINGER -> state = fingerMode
            }
            setYMax(touchPoint, touchPoint)
            if (state == ERASING) {
              toDel.clear()
              toAdd.clear()
            }
          }
          clearLasso()
          if (state != ERASING) {
            currentStroke = Stroke(paints[if (lassoMode) {LASSO} else {PEN}])
            currentStroke.addAll(arrayOf(touchPoint.x, touchPoint.y))
          }
        }
        MotionEvent.ACTION_MOVE -> {
          when (state) {
            MOVING -> {
              // Move all the lassoed strokes
              setYMax(lassoed.offset(touchPoint.x - prevPoint.x, touchPoint.y - prevPoint.y))
              // Also move the lasso itself
              currentStroke.offset(touchPoint.x - prevPoint.x, touchPoint.y - prevPoint.y)
            }
            DRAWING -> {
              currentStroke.addAll(arrayOf(touchPoint.x, touchPoint.y))
              if (!lassoMode) {
                setYMax(touchPoint, touchPoint)
              }
            }
            ERASING -> eraseMove(touchPoint)
          }
        }
        MotionEvent.ACTION_UP -> {
          down = false
          when (state) {
            SCROLLING -> { // We were scrolling, now we aren't.
              state = DRAWING
            }
            MOVING -> { // Stop move, do nothing else.
              state = DRAWING
              // Broadcast out changed strokes
              for ((key, stroke) in lassoed) {
                val tempP = stroke.paint
                stroke.paint = paints[PEN]
                broadcast(STROKE, key, stroke)
                stroke.paint = tempP
              }
            }
            ERASING -> eraseUp()
            DRAWING -> {
              if (lassoMode) {
                // Complete lasso
                currentStroke.addAll(arrayOf(downPoint.x, downPoint.y)) // Close lasso
                while (currentStroke.size < 8) { // Make sure it's big enough for pointinpoly
                                                 currentStroke.addAll(arrayOf(downPoint.x, downPoint.y))
                }
                // Mark all intersecting objects
                markLassoed()
                // Turn lasso border invisible
                currentStroke.paint = newPaint(Color.TRANSPARENT, 0f)
                // Put copy bubble on there
              } else {
                if (downPoint == prevPoint) { // Didn't move, leave a little dot
                  currentStroke.addAll(arrayOf(prevPoint.x+1, prevPoint.y+1))
                }
                commitDraw(currentStroke)
                currentStroke = Stroke(paints[PEN])
              }
            }
          }
        }
      }
      prevPoint = touchPoint
      invalidate()
      return true
    }
    // Two or more fingers
    when (event.actionMasked) {
      MotionEvent.ACTION_POINTER_DOWN -> {
        // Second finger hit, invalidate current stroke.
        currentStroke = Stroke(paints[PEN])
        state = SCROLLING
        _sy = event.y.toInt()
      }
      MotionEvent.ACTION_MOVE -> {
        scrollBy(0, _sy-event.y.toInt())
        _sy=event.y.toInt()
        if (scrollY < 0) {
          scrollY = 0
        }
        invalidate()
        return true
      }
      MotionEvent.ACTION_UP -> {
        down = false
        if (state == SCROLLING) { // We were moving, now we aren't.
            state = DRAWING
            return true
        }
      }
    }
    return false
  }

  // Unmark lassoed strokes and clear the lasso
  private fun clearLasso() {
    for ((_, stroke) in lassoed) {
      stroke.paint = paints[PEN]
    }
    lassoed.clear()
  }

  // Dump all points for testing.
  private fun dumpPoints() {
    Log.w(TAG, "DUMPING STROKE DB ----")
    for ((key, stroke) in strokes) {
      Log.w(TAG, "Stroke ${key}:")
      for (i in 0 until stroke.size-1 step 2) {
        Log.w(TAG, "  %.0f, %.0f".format(stroke[i], stroke[i+1]))
      }
    }
  }
}

typealias SimpleLambda = () -> Unit

class Undoable(val doit: SimpleLambda, val undo: SimpleLambda) {}

class Undo: MutableList<Undoable> by mutableListOf<Undoable>() {
  // Are there undos available?
  val undoable: Boolean
    get() {
      return index > 0
    }
  // Are there redos available?
  val redoable: Boolean
    get() {
      return index < size
    }

  var index = 0

  // Add to stack without doing the doit part.
  fun silentDoit(todo: SimpleLambda, undo: SimpleLambda) {
    // If there was an undo, a doit removes all history after the undo.
    while (redoable) {
      removeAt(index)
    }
    add(Undoable(todo, undo))
    index = size
  }

  // Add to stack and execut the doit lambda
  fun doit(todo: SimpleLambda, undo: SimpleLambda) {
    silentDoit(todo, undo)
    todo()
  }
  fun undo() {
    if (!undoable) return
    index--
    this[index].undo()
  }
  fun redo() {
    if (!redoable) return
    this[index].doit()
    index++
  }
}


@Entity(tableName = "strokes")
data class StrokeData(
    @PrimaryKey val id: String,
    @ColumnInfo(name = "stroke") val stroke: Stroke
)

@Dao
interface StrokeDao {
    @Query("SELECT * FROM strokes")
    fun getAll(): List<StrokeData>

    @Insert
    fun insertAll(vararg strokes: StrokeData)
}

@Database(entities = arrayOf(StrokeData::class), version = 1, exportSchema = false)
@TypeConverters(DataConverter::class)
abstract class StrokeDatabase : RoomDatabase() {
  abstract fun strokeDao(): StrokeDao
}

class DataConverter {
  @TypeConverter
  fun fromStroke(value: Stroke): String {
    val bos = ByteArrayOutputStream()
    val os = ObjectOutputStream(bos)
    os.writeObject(value)
    os.close()
    return bos.toByteArray().toHexString()
  }

  @TypeConverter
  fun toStroke(value: String): Stroke {
    val bis = ByteArrayInputStream(value.hexToByteArray())
    val oInputStream =  ObjectInputStream(bis)
    return oInputStream.readObject() as Stroke
  }
}


/* Next Steps:

1) Lasso selection with move/copy/paste/clear

2) Velocity-sensitive erasing.

3) Change right-hand screen icons to arrows (double up/down for
   start/end, single up/down for pgup/pgdown)

4) Palette of pasteable items.

5) Board save/load

6) Rearrangement of slices (i.e. "slide rearrangement")

7) Mathy tools: graph building, geometry, etc.


One nice thing: selecting pen size slot. Tap to select size, or hold
to bring up a ruler that you can slide to set the pen size for that
slot. IN addition to the ruler there's a little dot that shows the
size of the pen you're making.  Maybe make it so holding gives you the
ability to change both size and color. It also brings up a control to
add another pen slot.


*/





/* Erase strategy.

Proof of concept:

Use a square. Check to see if either point of the line is in the
box. If so, add erasure to the undo stack and commit.

Do this on each move.

*/
