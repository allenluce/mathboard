package com.prometheanworld.mathboard

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import android.graphics.Paint

@RunWith(AndroidJUnit4::class)
@LargeTest
class SerialTest {
  @Test fun serializeStroke() {
    val paint = Paint()
    val obj = Stroke(paint)
    obj.addAll(arrayOf(151f, 157f, 139f, 149f))
    
    val outData = ByteArrayOutputStream()
    val out = ObjectOutputStream(outData)
    out.writeObject(obj)
    out.close()

    val inData = outData.toByteArray()
    var inStream = ObjectInputStream(ByteArrayInputStream(inData))
    val recoveredObj = inStream.readObject() as Stroke
    assertEquals(recoveredObj[0], 151f)
    assertEquals(recoveredObj[1], 157f)
    assertEquals(recoveredObj[2], 139f)
    assertEquals(recoveredObj[3], 149f)
    assertEquals(recoveredObj.size, 4)
    assertEquals(recoveredObj.paint.color, paint.color)
    assertEquals(recoveredObj.paint.strokeWidth, paint.strokeWidth)
  }
}
