package com.prometheanworld.mathboard

import org.junit.Assert.assertEquals
import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Paint
import android.os.SystemClock
import android.view.InputDevice
import android.view.MotionEvent
import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.UiDevice
import java.io.File
import java.io.FileOutputStream
import org.hamcrest.Matcher
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class BoardTest {
  @get:Rule
  val activityRule = ActivityScenarioRule(MainActivity::class.java)

  val context = InstrumentationRegistry.getInstrumentation().getTargetContext()
  var boardView: View? = null
  var view: ViewInteraction? = null

  @Before fun before() {
    activityRule.scenario.onActivity {
      boardView = it?.boardView
    }
    view = onView(withId(R.id.view_whiteboard))
  }

  // Test point-in-polygon method
  @Test fun containsTest() {
    val s = Stroke(Paint())
    s.addAll(arrayOf(10f,10f, 10f,100f, 100f,100f, 100f,10f, 10f,10f))
    assertTrue(s.contains(20f,20f))
    assertTrue(s.contains(90f,90f))
    assertFalse(s.contains(101f,101f))
    assertFalse(s.contains(8f,1f))
  }

  @Test fun simpleLine() {
    view?.perform(Line(0f,0f, 20f,10f, 40f,12f, 50f,20f, 100f,100f))
    sameAs("lineTest.png")
  }

  @Test fun fourStrokes() {
    for (stroke in Strokes()) {
      view?.perform(Line(*stroke))
    }
    sameAs("fourStrokes.png")
  }

  @Test fun eraseStroke() {
    // Four strokes
    for (stroke in Strokes()) {
      view?.perform(Line(*stroke))
    }
    // An erase stroke.
    view?.perform(Line(*Strokes().erase1).withFinger())
    sameAs("eraseStroke.png")
  }

  // Make sure the erase cursor shows up when appropriate.
  @Test fun eraseCursor() {
    // Four strokes
    for (stroke in Strokes()) {
      view?.perform(Line(*stroke))
    }
    // A system of a down, cursor should appear.
    view?.perform(Line(10f, 10f).withFinger().noUp())
    sameAs("eraseDown.png")

    // Cursor should move
    view?.perform(Line(20f, 20f, 30f, 30f, 40f, 40f).withFinger().noUp().noDown())
    sameAs("eraseMove.png")

    // On up, cursor disappears.
    view?.perform(Line(50f, 50f, 60f, 60f).withFinger().noDown())
    sameAs("eraseUp.png")
  }

  // Make sure the erase cursor shows up when scrolled
  @Test fun scrollEraseCursor() {
    // Scroll down a bunch
    view?.perform(TwoFinger(50f, 200f, 50f, 10f, 50f, 5f))
    assertEquals(195, boardView!!.scrollY)
    // Four strokes
    for (stroke in Strokes()) {
      view?.perform(Line(*stroke))
    }
    // On down, cursor should appear.
    view?.perform(Line(10f, 10f).withFinger().noUp())
    sameAs("eraseDown.png")

    // Cursor should move
    view?.perform(Line(20f, 20f, 30f, 30f, 40f, 40f).withFinger().noUp().noDown())
    sameAs("scrollEraseMove.png") // Very slight scaling-related difference from non-scrolled

    // On up, cursor disappears.
    view?.perform(Line(50f, 50f, 60f, 60f).withFinger().noDown())
    sameAs("eraseUp.png")
  }

  @Test fun multiEraseStroke() {
    // Four strokes
    for (stroke in Strokes()) {
      view?.perform(Line(*stroke))
    }
    // First erase
    view?.perform(Line(*Strokes().erase1).withFinger())
    // First stroke
    view?.perform(Line(*Strokes().stroke1))
    // Second erase
    view?.perform(Line(*Strokes().erase2).withFinger())
    // Second stroke
    view?.perform(Line(*Strokes().stroke2))
    sameAs("multiEraseStroke1.png")

    // Undo the second stroke.
    onView(withText("undo")).perform(click())
    // Undo the second erase.
    onView(withText("undo")).perform(click())
    sameAs("multiEraseStroke2.png")

    // Undo the first stroke.
    onView(withText("undo")).perform(click())
    // Undo the first erase.
    onView(withText("undo")).perform(click())
    sameAs("fourStrokes.png")

    // Redo the first erase.
    onView(withText("redo")).perform(click())
    // Redo the first stroke.
    onView(withText("redo")).perform(click())
    sameAs("multiEraseStroke2.png")

    // Redo the second erase.
    onView(withText("redo")).perform(click())
    // Redo the second stroke.
    onView(withText("redo")).perform(click())
    sameAs("multiEraseStroke1.png")

    // Redo a bunch more
    repeat (5) {
      onView(withText("redo")).perform(click())
    }
    sameAs("multiEraseStroke1.png")

    // Undo way more than necessary.
    repeat (50) {
      onView(withText("undo")).perform(click())
    }
    sameAs("blank.png")
  }

  // Double-finger move board up a bit.
  @Test fun moveDown() {
    for (stroke in Strokes()) {
      view?.perform(Line(*stroke))
    }
    assertEquals(boardView!!.scrollY, 0)
    view?.perform(TwoFinger(50f, 90f, 50f, 70f, 50f, 40f, 50f, 10f))
    assertEquals(boardView!!.scrollY, 80)
  }

  // Check that the lasso starts drawing properly.
  @Test fun startLasso() {
    // Put down some strokes
    for (stroke in Strokes()) {
      view?.perform(Line(*stroke))
    }
    // Click the lasso button
    onView(withText("lasso")).perform(click())
    // Draw a bunch of points
    val partialLasso = Strokes().lasso.sliceArray(1..100)
    view?.perform(Line(*partialLasso).noUp())

    sameAs("startLasso.png")
  }

  // Check a full lasso draw
  @Test fun fullLasso() {
    // Put down some strokes
    for (stroke in Strokes()) {
      view?.perform(Line(*stroke))
    }
    // Click the lasso button
    onView(withText("lasso")).perform(click())
    // Draw the lasso.
    view?.perform(Line(*Strokes().lasso))
    sameAs("fullLasso.png")
  }

  // Move lassoed strokes
  @Test fun moveLasso() {
    // Put down some strokes
    for (stroke in Strokes()) {
      view?.perform(Line(*stroke))
    }
    // Click the lasso button
    onView(withText("lasso")).perform(click())
    // Draw the lasso.
    view?.perform(Line(*Strokes().lasso))
    // Click in the middle of the lasso and move down 20 pixels.
    view?.perform(Line(100f, 100f, 100f, 110f, 100f, 120f))
    sameAs("moveLasso.png")
  }

  // Move lassoed strokes and click elsewhere to turn off lasso
  @Test fun moveClickLasso() {
    // Put down some strokes
    for (stroke in Strokes()) {
      view?.perform(Line(*stroke))
    }
    // Click the lasso button
    onView(withText("lasso")).perform(click())
    // Draw the lasso.
    view?.perform(Line(*Strokes().lasso))
    // Click in the middle of the lasso and move down 20 pixels.
    view?.perform(Line(100f, 100f, 100f, 110f, 100f, 120f))
    // Click to turn lasso off.
    view?.perform(Line(1f, 1f))
    sameAs("moveClickLasso.png")
  }

  // Move lassoed strokes and click to turn off lasso
  @Test fun moveClickLassoButton() {
    // Put down some strokes
    for (stroke in Strokes()) {
      view?.perform(Line(*stroke))
    }
    // Click the lasso button
    onView(withText("lasso")).perform(click())
    // Draw the lasso.
    view?.perform(Line(*Strokes().lasso))
    // Click in the middle of the lasso and move down 20 pixels.
    view?.perform(Line(100f, 100f, 100f, 110f, 100f, 120f))
    // Click the lasso button again
    onView(withText("lasso")).perform(click())
    sameAs("moveClickLasso.png")
  }

  private fun sameAs(fileName: String) {
    val bitmap = getBitmap(boardView!!)
    //saveClip(bitmap)
    assertTrue(bitmap.sameAs(assetBitmap(fileName)))
  }

  // Load up a pre-rendered bitmap from the assets
  private fun assetBitmap(name: String): Bitmap {
    val str = context.getAssets().open(name)
    return BitmapFactory.decodeStream(str)
  }    

  // Grab the 200x200 upper-left portion of the screen.
  private fun getBitmap(view: View): Bitmap {
    var bitmap = Bitmap.createBitmap(200, 200+view.scrollY, Bitmap.Config.ARGB_8888)
    view.draw(Canvas(bitmap))
    if (view.scrollY > 0) { // We're scrolled, crop it.
      bitmap = Bitmap.createBitmap(bitmap, 0, view.scrollY, 200, 200)
    }
    return bitmap
  }
  
  // Utility method to save clips for use in tests
  private fun saveClip(bitmap: Bitmap) {
    val outFile = File(context.getCacheDir(), "wb.png")
    val out = FileOutputStream(outFile)
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
    out.close()
    Thread.sleep(10000)
  }

  // Utility method to take a screenshot
  private fun screenShot(to: File) {
    val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    device.takeScreenshot(to)
  }
}

class Line(vararg val line: Float): ViewAction {
  var up = true
  var down = true
  var toolType = MotionEvent.TOOL_TYPE_STYLUS

  override fun getDescription() = "send touch events"
  override fun getConstraints() = isDisplayed()
  override fun perform(uiController:UiController, view:View) {
    val downTime = SystemClock.uptimeMillis()
    val inputDevice = InputDevice.SOURCE_TOUCHSCREEN
    
    val pointerProperties = arrayOf(MotionEvent.PointerProperties())
    pointerProperties[0].clear()
    pointerProperties[0].id = 0
    pointerProperties[0].toolType = toolType
    
    val event =  MotionEvent.obtain(downTime, downTime, MotionEvent.ACTION_DOWN,
      1, // pointerCount
      pointerProperties,
      arrayOf(MotionEvent.PointerCoords()),
      0, // metaState
      MotionEvent.BUTTON_PRIMARY, // buttonState
      1f, // precision
      1f, // precision
      0, // deviceId
      0, // edgeFlags
      inputDevice,
      0) // flags

    event.setLocation(line[0], line[1])

    if (down) {
      view.onTouchEvent(event)
    }

    event.setAction(MotionEvent.ACTION_MOVE)
    for(i in 2 until line.size - 1 step 2) {
      event.setLocation(line[i], line[i+1])
      view.onTouchEvent(event)
    }

    if (up) {
      event.setAction(MotionEvent.ACTION_UP)
      view.onTouchEvent(event)
    }
  }

  fun withFinger(): Line {
    toolType = MotionEvent.TOOL_TYPE_FINGER
    return this
  }

  fun noUp(): Line {
    up = false
    return this
  }

  fun noDown(): Line {
    down = false
    return this
  }
}

class TwoFinger(vararg val line: Float): ViewAction {
  override fun getDescription() = "send double-finger touch events"
  override fun getConstraints() = isDisplayed()
  override fun perform(uiController:UiController, view:View) {
    val downTime = SystemClock.uptimeMillis()
    val inputDevice = InputDevice.SOURCE_TOUCHSCREEN

    val pointerProperties = arrayOf(MotionEvent.PointerProperties(), MotionEvent.PointerProperties())
    pointerProperties[0].clear()
    pointerProperties[0].id = 0
    pointerProperties[0].toolType = MotionEvent.TOOL_TYPE_FINGER
    pointerProperties[1].clear()
    pointerProperties[1].id = 1
    pointerProperties[1].toolType = MotionEvent.TOOL_TYPE_FINGER

    // Two tiny fingers, 10 pixels away from each other.
    val pointerCoords = arrayOf(coords(line[0], line[1]), coords(line[0]+10, line[1]))

    val event =  MotionEvent.obtain(downTime, downTime, MotionEvent.ACTION_POINTER_DOWN,
      2, // pointerCount
      pointerProperties,
      pointerCoords,
      0, // metaState
      MotionEvent.BUTTON_PRIMARY, // buttonState
      1f, // precision
      1f, // precision
      0, // deviceId
      0, // edgeFlags
      inputDevice,
      0) // flags

    view.onTouchEvent(event)

    event.setAction(MotionEvent.ACTION_MOVE)
    for(i in 2 until line.size - 1 step 2) {
      event.addBatch(SystemClock.uptimeMillis(), arrayOf(coords(line[i], line[i+1]), coords(line[i]+10, line[i+1])), 0)
      view.onTouchEvent(event)
    }
    event.setAction(MotionEvent.ACTION_UP)
    view.onTouchEvent(event)
  }

  private fun coords(x: Float, y: Float): MotionEvent.PointerCoords {
    val pointerCoords = MotionEvent.PointerCoords()
    pointerCoords.clear()
    pointerCoords.x = x
    pointerCoords.y = y
    pointerCoords.pressure = 0f
    pointerCoords.size = 1f
    return pointerCoords
  }
}

class Strokes: Iterator<FloatArray> {
  private var i = 0
  override operator fun hasNext() = i < 4
  override operator fun next() = strokes[i++]

  val strokes = arrayOf(
    floatArrayOf(
      14f, 91f, 14f, 92f, 16f, 93f, 21f, 98f, 30f, 109f, 32f, 114f,
      34f, 116f, 34f, 117f, 36f, 120f, 36f, 121f),
    floatArrayOf(
      41f, 76f, 43f, 82f, 72f, 110f, 83f, 119f, 85f, 125f, 86f, 124f),
    floatArrayOf(
      83f, 65f, 88f, 73f, 106f, 92f, 111f, 96f, 115f, 100f),
    floatArrayOf(
      115f, 36f, 116f, 37f, 116f, 38f, 118f, 40f, 130f, 53f, 135f, 57f))

  val lasso = floatArrayOf(
    134f, 95f, 133f, 91f, 130f, 83f, 127f, 76f, 125f, 75f, 123f,
    73f, 121f, 72f, 117f, 71f, 114f, 68f, 109f, 65f, 107f, 64f, 105f,
    62f, 104f, 62f, 100f, 60f, 95f, 55f, 93f, 54f, 89f, 52f, 85f,
    50f, 84f, 49f, 82f, 48f, 81f, 47f, 78f, 47f, 74f, 47f, 73f, 47f,
    70f, 46f, 68f, 46f, 66f, 46f, 63f, 46f, 58f, 46f, 56f, 46f, 53f,
    46f, 50f, 46f, 46f, 46f, 45f, 47f, 40f, 49f, 37f, 49f, 37f, 49f,
    34f, 52f, 32f, 53f, 31f, 55f, 30f, 55f, 27f, 57f, 27f, 59f, 26f,
    61f, 26f, 62f, 25f, 62f, 25f, 64f, 26f, 65f, 26f, 67f, 27f, 68f,
    27f, 72f, 27f, 75f, 28f, 76f, 29f, 77f, 29f, 81f, 30f, 83f, 30f,
    84f, 31f, 85f, 32f, 87f, 32f, 89f, 33f, 90f, 33f, 91f, 34f, 95f,
    37f, 98f, 37f, 101f, 38f, 103f, 41f, 104f, 43f, 107f, 44f, 108f,
    44f, 110f, 45f, 111f, 47f, 113f, 47f, 113f, 49f, 116f, 50f, 119f,
    53f, 121f, 53f, 123f, 55f, 124f, 61f, 128f, 64f, 129f, 67f, 131f,
    69f, 132f, 70f, 132f, 75f, 134f, 81f, 136f, 86f, 137f, 85f, 136f,
    87f, 137f, 88f, 137f, 96f, 137f, 105f, 136f, 105f, 135f, 109f,
    114f, 132f, 112f, 134f, 110f, 136f, 109f, 138f, 108f, 142f, 106f,
    144f, 103f, 146f, 102f)

  // Stroke/erase test strokes, done after the four strokes above.

  val erase1 = floatArrayOf(
    27f, 61f, 30f, 61f, 30f, 62f, 32f, 62f, 35f, 62f, 41f, 60f, 43f,
    59f, 45f, 58f, 48f, 56f, 51f, 55f, 52f, 54f, 54f, 53f, 55f, 53f,
    54f, 52f, 57f, 52f, 58f, 51f, 61f, 50f, 63f, 49f, 64f, 48f, 65f,
    48f, 66f, 47f, 68f, 46f, 68f, 46f, 69f, 45f, 71f, 44f, 71f, 44f,
    72f, 44f, 73f, 43f, 76f, 42f, 78f, 41f, 80f, 40f, 81f, 38f)

  val stroke1 = floatArrayOf(
    21f, 94f, 25f, 93f, 27f, 93f, 27f, 92f, 28f, 92f, 28f, 92f, 30f,
    91f, 33f, 89f, 34f, 88f, 42f, 86f, 43f, 84f, 45f, 84f, 48f, 82f,
    49f, 82f, 52f, 80f, 54f, 78f, 57f, 76f, 58f, 76f, 62f, 74f, 63f,
    73f, 65f, 72f, 66f, 71f, 68f, 69f, 69f, 68f, 74f, 65f, 76f, 63f,
    80f, 62f, 83f, 60f, 87f, 56f, 88f, 55f, 93f, 52f, 94f, 52f, 97f,
    49f, 100f, 48f, 103f, 46f, 107f, 44f, 109f, 43f, 110f, 42f, 112f,
    41f, 114f, 40f, 117f, 36f, 119f, 36f, 121f, 35f, 124f, 32f, 125f,
    31f, 126f, 31f)

  val erase2 = floatArrayOf(
    46f, 35f, 46f, 36f, 46f, 37f, 47f, 38f, 47f, 40f, 48f, 40f, 48f,
    42f, 48f, 42f, 49f, 42f, 49f, 43f, 50f, 43f, 50f, 44f, 51f, 44f,
    52f, 45f, 53f, 46f, 55f, 46f, 54f, 47f, 56f, 47f, 56f, 48f, 57f,
    49f, 58f, 50f, 58f, 51f, 61f, 51f, 60f, 52f, 61f, 53f, 62f, 54f,
    63f, 55f, 64f, 56f, 64f, 57f, 65f, 57f, 65f, 58f, 66f, 58f, 66f,
    61f, 66f, 61f, 67f, 61f, 67f, 62f, 68f, 62f, 68f, 63f, 68f, 64f,
    69f, 64f, 69f, 65f, 70f, 65f, 70f, 66f)

  val stroke2 = floatArrayOf(
    47f, 112f, 48f, 112f, 50f, 110f, 52f, 109f, 55f, 100f, 56f, 97f,
    56f, 95f, 58f, 90f, 62f, 85f, 63f, 79f, 66f, 70f, 73f, 57f, 76f,
    53f, 79f, 49f, 83f, 43f, 87f, 35f, 89f, 31f, 93f, 26f, 95f, 24f,
    102f, 20f, 101f, 18f, 103f, 17f)
}
